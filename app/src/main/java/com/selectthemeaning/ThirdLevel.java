package com.selectthemeaning;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.selectthemeaning.data.Question;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tasos Taptas on 3/9/2018.
 */

public class ThirdLevel extends AppCompatActivity {

    private RecyclerView recycler;
    private ThirdLevelAdapter adapter;
    private List<Question> data = new ArrayList<>();
    private Button btnNext;
    private Toolbar toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.third_level);

        toolbar = findViewById(R.id.toolbar);
        recycler = findViewById(R.id.recycler);
        btnNext = findViewById(R.id.btn_next);


        makeData();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeConfirmDialog();
            }
        });

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Τρίτο επίπεδο");
        }
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    private void getCorrectAnswers() {
        Utils.thirdLevelAnswers = 0;


        for(Question question : data) {
            if(question.isMetaphor() == question.isUserAnswerIsMetaphor() && !question.getUserHasAnswered().equals("")) {
                Utils.thirdLevelAnswers++;
            }
        }


    }

    private void makeConfirmDialog() {
        getCorrectAnswers();
        new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle("Επαλήθευση αποτελεσμάτων")
                .setMessage("Απάντησες σωστά σε " + Utils.thirdLevelAnswers+"/14 ερωτήσεις.\n"+(Utils.thirdLevelAnswers < 7 ? "Θέλεις να προσπαθήσεις ξανά;" : "Θέλεις να προχωρήσεις;") )
                .setPositiveButton("NAI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(Utils.thirdLevelAnswers < 7) {
                        }
                        else{
                            Utils.dataThirdLevel.addAll(data);
                            startActivity(new Intent(ThirdLevel.this, ForthLevel.class));
                        }

                    }
                })
                .setNegativeButton("ΟΧΙ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(Utils.thirdLevelAnswers < 7) {
                            Utils.dataThirdLevel.addAll(data);
                            startActivity(new Intent(ThirdLevel.this, ForthLevel.class));
                        }
                        else {

                        }
                    }
                })
                .show();
    }


    private void makeData() {
        Utils.thirdLevelAnswers = 0;
        Question question = new Question();
        question.setQuestion("• Πήραν τα μυαλά του αέρα");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Η θάλασσα ήταν βαθειά");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Τα έχω κάνει θάλασσα");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Μη μασάς τα λόγια σου");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Τα λόγια σου έσταζαν μέλι");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Το μέλι είναι γλυκό");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Στην τάξη άναψε η συζήτηση");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Σκοτείνιασε ο ουρανός");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Σκοτείνιασε το βλέμα του");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Σίφουνας έγινε");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Αυτός πετάει στα σύννεφα");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Το αεροπλάνο πετάει");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Μεγάλη ιδέα έχεις για τον εαυτό σου");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Μάλλιασε η γλώσσα μου");
        question.setMetaphor(true);

        data.add(question);

        adapter = new ThirdLevelAdapter(data, this, 0);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(mLayoutManager);
        recycler.setAdapter(adapter);


    }




}
