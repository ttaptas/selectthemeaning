package com.selectthemeaning;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.selectthemeaning.data.MetaphorQuestion;
import com.selectthemeaning.data.QuestionWithAnswers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Tasos on 10/03/2018.
 */

public class FinalLevel extends AppCompatActivity {

    private Button btnNext;
    private TextView tvFirst, tvSecond, tvThird, tvForth, tvFifth, tvSixth, tvSeventh, tvEighth;
    private TextView tvFirstAnswer, tvSecondAnswer, tvThirdAnswer, tvForthAnswer, tvFifthAnswer,
            tvSixthAnswer, tvSeventhAnswer, tvEighthAnswer;

    List<QuestionWithAnswers> data = new ArrayList<>();
    List<String> possibleAnswers = new ArrayList<>();

    private int preSelectedAnswer;
    private CharSequence[] answers;
    private Toolbar toolbar;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.final_level);

        makeData();

        toolbar = findViewById(R.id.toolbar);
        btnNext = findViewById(R.id.btn_next);
        tvFirst = findViewById(R.id.tv_first);
        tvFirstAnswer = findViewById(R.id.tv_first_answer);
        tvFirstAnswer.setTag(tvFirst.getText().toString());
        tvFirstAnswer.setOnClickListener(answerListener);

        tvSecond = findViewById(R.id.tv_second);
        tvSecondAnswer = findViewById(R.id.tv_second_answer);
        tvSecondAnswer.setTag(tvSecond.getText().toString());
        tvSecondAnswer.setOnClickListener(answerListener);

        tvThird = findViewById(R.id.tv_third);
        tvThirdAnswer = findViewById(R.id.tv_third_answer);
        tvThirdAnswer.setTag(tvThird.getText().toString());
        tvThirdAnswer.setOnClickListener(answerListener);

        tvForth = findViewById(R.id.tv_forth);
        tvForthAnswer = findViewById(R.id.tv_forth_answer);
        tvForthAnswer.setTag(tvForth.getText().toString());
        tvForthAnswer.setOnClickListener(answerListener);

        tvFifth = findViewById(R.id.tv_fifth);
        tvFifthAnswer = findViewById(R.id.tv_fifth_answer);
        tvFifthAnswer.setTag(tvFifth.getText().toString());
        tvFifthAnswer.setOnClickListener(answerListener);

        tvSixth = findViewById(R.id.tv_sixth);
        tvSixthAnswer = findViewById(R.id.tv_sixth_answer);
        tvSixthAnswer.setTag(tvSixth.getText().toString());
        tvSixthAnswer.setOnClickListener(answerListener);

        tvSeventh = findViewById(R.id.tv_seventh);
        tvSeventhAnswer = findViewById(R.id.tv_seventh_answer);
        tvSeventhAnswer.setTag(tvSeventh.getText().toString());
        tvSeventhAnswer.setOnClickListener(answerListener);

        tvEighth = findViewById(R.id.tv_eighth);
        tvEighthAnswer = findViewById(R.id.tv_eighth_answer);
        tvEighthAnswer.setTag(tvEighth.getText().toString());
        tvEighthAnswer.setOnClickListener(answerListener);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeConfirmDialog();
            }
        });

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Τελικό επίπεδο");
        }
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }


    private View.OnClickListener answerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            makeAnswerDialog((TextView) v, ((String) v.getTag()));
        }
    };

    private void setPossibleAnswers(TextView view) {
        possibleAnswers.clear();
        for(QuestionWithAnswers questionWithAnswers : data) {
            if(questionWithAnswers.getQuestion().equals(view.getTag())) {
                possibleAnswers.addAll(questionWithAnswers.getAnswers());
            }
        }

        Collections.shuffle(possibleAnswers);
        findPreselectedAnswer(view.getText().toString());
        answers = possibleAnswers.toArray(new CharSequence[possibleAnswers.size()]);
    }

    private void findPreselectedAnswer(String input) {
        for (int i = 0; i < possibleAnswers.size(); i++) {
            if (possibleAnswers.get(i).equals(input)) {
                preSelectedAnswer = i;
                return;
            }
        }

        preSelectedAnswer = -1;
    }

    private void getCorrectAnswers() {
        Utils.finalLevelAnswers = 0;
        for(QuestionWithAnswers questionObj : data) {
            if(questionObj.getUserAnswer().equals(questionObj.getCorrectAnswer())) {
                Utils.finalLevelAnswers++;
            }
        }

    }

    private void makeConfirmDialog() {
        getCorrectAnswers();
        new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle("Επαλήθευση αποτελεσμάτων")
                .setMessage("Απάντησες σωστά σε " + Utils.finalLevelAnswers+"/8 ερωτήσεις.\n"+(Utils.finalLevelAnswers < 5 ? "Θέλεις να προσπαθήσεις ξανά;" : "Θέλεις να προχωρήσεις;") )
                .setPositiveButton("NAI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(Utils.finalLevelAnswers < 5) {
                        }
                        else{
                            Utils.dataFinalLevel.addAll(data);
                            startActivity(new Intent(FinalLevel.this, Results.class));

                        }

                    }
                })
                .setNegativeButton("ΟΧΙ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(Utils.finalLevelAnswers < 5) {
                            Utils.dataFinalLevel.addAll(data);
                            startActivity(new Intent(FinalLevel.this, Results.class));
                        }
                        else {

                        }
                    }
                })
                .show();
    }

    private void makeAnswerDialog(final TextView view, final String question) {
        setPossibleAnswers(view);

        new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle(question.replace("• ", ""))
                .setSingleChoiceItems(answers, preSelectedAnswer, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        preSelectedAnswer = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        view.setText(answers[preSelectedAnswer].toString());
                        validateData(question, answers[preSelectedAnswer].toString());
                    }
                })
                .show();
    }


    private void validateData(String question, String answer) {
        for(QuestionWithAnswers questionObj : data) {
            if(questionObj.getQuestion().equals(question)) {
                questionObj.setUserAnswer(answer);
                break;
            }
        }
    }

    private void makeData() {
        Utils.finalLevelAnswers = 0;

        QuestionWithAnswers questionWithAnswers = new QuestionWithAnswers();
        List<String> answers = new ArrayList<>();
        questionWithAnswers.setQuestion("• Κρέμεται από μια κλωστή.");
        answers.add("Είναι άρρωστος");
        answers.add("Είναι σε δύσκολη θέση");
        answers.add("Είναι μπερδεμένος");
        answers.add("Ράβει");
        questionWithAnswers.setAnswers(answers);
        questionWithAnswers.setCorrectAnswer("Είναι σε δύσκολη θέση");

        data.add(questionWithAnswers);

        questionWithAnswers = new QuestionWithAnswers();
        answers = new ArrayList<>();
        questionWithAnswers.setQuestion("• Ντύνεται στην τρίχα.");
        answers.add("Είναι πολύ κομψός");
        answers.add("Φοράει κουρέλια");
        answers.add("Είναι γεμάτος τρίχες");
        answers.add("Είναι πλούσιος");
        questionWithAnswers.setAnswers(answers);
        questionWithAnswers.setCorrectAnswer("Είναι πολύ κομψός");

        data.add(questionWithAnswers);

        questionWithAnswers = new QuestionWithAnswers();
        answers = new ArrayList<>();
        questionWithAnswers.setQuestion("• Του βγάζω τ'απλυτα στη φόρα.");
        answers.add("Λεω αλήθεια");
        answers.add("Λεω ψέμματα");
        answers.add("Φανερώνω πράγματα για κάποιον");
        answers.add("Βάζω πληντύριο");
        questionWithAnswers.setAnswers(answers);
        questionWithAnswers.setCorrectAnswer("Φανερώνω πράγματα για κάποιον");

        data.add(questionWithAnswers);

        questionWithAnswers = new QuestionWithAnswers();
        answers = new ArrayList<>();
        questionWithAnswers.setQuestion("• Πετάει στα σύννεφα.");
        answers.add("Μπήκε στο αεροπλάνο");
        answers.add("Ονειροπολεί");
        answers.add("Βρίσκεται σε ψηλό σημείο");
        answers.add("Είναι καλοπροαίρετος");
        questionWithAnswers.setAnswers(answers);
        questionWithAnswers.setCorrectAnswer("Ονειροπολεί");

        data.add(questionWithAnswers);

        questionWithAnswers = new QuestionWithAnswers();
        answers = new ArrayList<>();
        questionWithAnswers.setQuestion("• Είναι σουπιά.");
        answers.add("Είναι έξυπνος και πονηρός");
        answers.add("Είναι μικροκαμωμένος");
        answers.add("Ζει στη θάλασσα");
        answers.add("Πετάει μελάνι");
        questionWithAnswers.setAnswers(answers);
        questionWithAnswers.setCorrectAnswer("Είναι έξυπνος και πονηρός");

        data.add(questionWithAnswers);

        questionWithAnswers = new QuestionWithAnswers();
        answers = new ArrayList<>();
        questionWithAnswers.setQuestion("• Κόβει το μάτι του.");
        answers.add("Κοιτάει με κακό τρόπο τους άλλους");
        answers.add("Έχει μεγάλα μάτια");
        answers.add("Πονάει το μάτι του");
        answers.add("Είναι παρατηρητικός και έξυπνος");
        questionWithAnswers.setAnswers(answers);
        questionWithAnswers.setCorrectAnswer("Είναι παρατηρητικός και έξυπνος");

        data.add(questionWithAnswers);

        questionWithAnswers = new QuestionWithAnswers();
        answers = new ArrayList<>();
        questionWithAnswers.setQuestion("• Κάνει την πάπια.");
        answers.add("Κολυμπάει σε λίμνες");
        answers.add("Κάνει τον ανήξερο για κάτι");
        answers.add("Κουνιέται πέρα δώθε");
        answers.add("Κάνει τον όμορφο");
        questionWithAnswers.setAnswers(answers);
        questionWithAnswers.setCorrectAnswer("Κάνει τον ανήξερο για κάτι");

        data.add(questionWithAnswers);

        questionWithAnswers = new QuestionWithAnswers();
        answers = new ArrayList<>();
        questionWithAnswers.setQuestion("• Έχει τις μαύρες του.");
        answers.add("Έχει μαύρα μάτια");
        answers.add("Λερώθηκε με μαύρη μπογιά");
        answers.add("Είναι στεναχωρημένος");
        answers.add("Ντύθηκε στα μαύρα");
        questionWithAnswers.setAnswers(answers);
        questionWithAnswers.setCorrectAnswer("Είναι στεναχωρημένος");

        data.add(questionWithAnswers);

    }
}

