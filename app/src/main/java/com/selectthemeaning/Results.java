package com.selectthemeaning;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Tasos on 11/03/2018.
 */

public class Results extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView tvFirstResults, tvSecondResults, tvThirdResults,
    tvForthResults, tvFifthResults, tvSixthResults, tvSeventhResults, tvPercentage;
    private Button btnRetry, btnFinish;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.results);


        toolbar = findViewById(R.id.toolbar);
        tvFirstResults = findViewById(R.id.tv_first_results);
        tvSecondResults = findViewById(R.id.tv_second_results);
        tvThirdResults = findViewById(R.id.tv_third_results);
        tvForthResults = findViewById(R.id.tv_forth_results);
        tvFifthResults = findViewById(R.id.tv_fifth_results);
        tvSixthResults = findViewById(R.id.tv_sixth_results);
        tvSeventhResults = findViewById(R.id.tv_seventh_results);
        tvPercentage = findViewById(R.id.tv_final_results);
        btnRetry = findViewById(R.id.btn_restart);
        btnFinish = findViewById(R.id.btn_finish);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Αποτελέσματα");
        }
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);


        tvFirstResults.setText("1o επίπεδο: " + Utils.firstLevelAnswers + "/8");
        tvSecondResults.setText("2o επίπεδο: " + Utils.secondLevelAnswers + "/5");
        tvThirdResults.setText("3o επίπεδο: " + Utils.thirdLevelAnswers + "/14");
        tvForthResults.setText("4o επίπεδο: " + Utils.forthLevelAnswers + "/16");
        tvFifthResults.setText("5o επίπεδο: " + Utils.fifthLevelAnswers + "/11");
        tvSixthResults.setText("6o επίπεδο: " + Utils.sixthLevelAnswers + "/16");
        tvSeventhResults.setText("7o επίπεδο: " + Utils.finalLevelAnswers + "/8");

        tvPercentage.setText("Ποσοστό: " + calculatePercentage());

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Results.this, FirstLevel.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                Results.this.finish();
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishAffinity();
            }
        });

    }


    private String calculatePercentage() {
        int sum = Utils.firstLevelAnswers + Utils.secondLevelAnswers + Utils.thirdLevelAnswers +
                Utils.forthLevelAnswers + Utils.fifthLevelAnswers + Utils.sixthLevelAnswers +
                Utils.finalLevelAnswers;

        int total = 8 + 5 + 14 + 16 + 11 + 16 + 8;


        long percentage = (sum * 100) / total ;
        return percentage+"%";
    }
}
