package com.selectthemeaning.data;

import java.io.Serializable;

/**
 * Created by Tasos on 04/03/2018.
 */

public class Question implements Serializable {

    private String question;
    private boolean isMetaphor;
    private boolean userAnswerIsMetaphor;
    private String userHasAnswered = "";

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public boolean isMetaphor() {
        return isMetaphor;
    }

    public void setMetaphor(boolean metaphor) {
        isMetaphor = metaphor;
    }

    public boolean isUserAnswerIsMetaphor() {
        return userAnswerIsMetaphor;
    }

    public void setUserAnswerIsMetaphor(boolean userAnswerIsMetaphor) {
        this.userAnswerIsMetaphor = userAnswerIsMetaphor;
    }

    public String getUserHasAnswered() {
        return userHasAnswered;
    }

    public void setUserHasAnswered(String userHasAnswered) {
        this.userHasAnswered = userHasAnswered;
    }
}
