package com.selectthemeaning.data;

import java.io.Serializable;

/**
 * Created by Tasos on 04/03/2018.
 */

public class MetaphorQuestion implements Serializable {

    private String question;
    private String correctMetaphor;
    private String userAnswer = "";

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCorrectMetaphor() {
        return correctMetaphor;
    }

    public void setCorrectMetaphor(String correctMetaphor) {
        this.correctMetaphor = correctMetaphor;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }
}
