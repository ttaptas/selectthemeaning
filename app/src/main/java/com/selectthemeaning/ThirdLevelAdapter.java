package com.selectthemeaning;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.selectthemeaning.data.Question;

import java.util.List;

/**
 * Created by Tasos Taptas on 3/9/2018.
 */

public class ThirdLevelAdapter extends RecyclerView.Adapter<ThirdLevelAdapter.MyViewHolder> {

    private List<Question> data;
    private Context context;
    private int type;

    ThirdLevelAdapter(List<Question> data, Context context, int type) {
        this.data = data;
        this.context = context;
        this.type = type;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int resourceId;
        if(type == 0) {
            resourceId = R.layout.question_item;
        }
        else if(type == 1) {
            resourceId = R.layout.question_check;
        }
        else {
            resourceId = R.layout.question_select;
        }

        View itemView = LayoutInflater.from(parent.getContext()).inflate(resourceId, parent, false);
        return new ThirdLevelAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        holder.tvQuestion.setText(data.get(position).getQuestion());

        if(type == 0){
            holder.tvMetafora.setTag(position);
            holder.tvKurioleksia.setTag(position);

            if(data.get(position).isUserAnswerIsMetaphor()) {
                holder.tvMetafora.setTextColor(Color.WHITE);
                holder.tvMetafora.setBackground(ContextCompat.getDrawable(context, R.drawable.square_border_color_accent));

                holder.tvKurioleksia.setTextColor(Color.parseColor("#565456"));
                holder.tvKurioleksia.setBackground(ContextCompat.getDrawable(context, R.drawable.square_border));
            }
            else{
                if(!data.get(position).getUserHasAnswered().equals("")) {
                    holder.tvKurioleksia.setTextColor(Color.WHITE);
                    holder.tvKurioleksia.setBackground(ContextCompat.getDrawable(context, R.drawable.square_border_color_accent));

                    holder.tvMetafora.setTextColor(Color.parseColor("#565456"));
                    holder.tvMetafora.setBackground(ContextCompat.getDrawable(context, R.drawable.square_border));
                }
                else{
                    holder.tvMetafora.setTextColor(Color.parseColor("#565456"));
                    holder.tvMetafora.setBackground(ContextCompat.getDrawable(context, R.drawable.square_border));

                    holder.tvKurioleksia.setTextColor(Color.parseColor("#565456"));
                    holder.tvKurioleksia.setBackground(ContextCompat.getDrawable(context, R.drawable.square_border));
                }
            }


            holder.tvMetafora.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    data.get((int)v.getTag()).setUserAnswerIsMetaphor(((TextView) v).getCurrentTextColor() == Color.parseColor("#565456"));
                    data.get((int)v.getTag()).setUserHasAnswered(((TextView) v).getCurrentTextColor() == Color.parseColor("#565456") ? "true" : "");
                    notifyDataSetChanged();
                }
            });

            holder.tvKurioleksia.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    data.get((int)v.getTag()).setUserAnswerIsMetaphor(false);
                    data.get((int)v.getTag()).setUserHasAnswered(((TextView) v).getCurrentTextColor() == Color.parseColor("#565456") ? "true" : "");
                    notifyDataSetChanged();
                }
            });
        }
        else if(type == 1){

            holder.cbKurioleksia.setTag(position);
            holder.cbMetafora.setTag(position);

            if(data.get(position).isUserAnswerIsMetaphor()) {
                holder.cbMetafora.setChecked(true);
                holder.cbKurioleksia.setChecked(false);
            }
            else{
                if(!data.get(position).getUserHasAnswered().equals("")) {
                    holder.cbMetafora.setChecked(false);
                    holder.cbKurioleksia.setChecked(true);
                }
                else{
                    holder.cbMetafora.setChecked(false);
                    holder.cbKurioleksia.setChecked(false);
                }
            }


            holder.cbMetafora.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    data.get((int)view.getTag()).setUserAnswerIsMetaphor(((CheckBox) view).isChecked());
                    data.get((int)view.getTag()).setUserHasAnswered(((CheckBox) view).isChecked() ? "true" : "");
                    notifyDataSetChanged();
                }
            });

            holder.cbKurioleksia.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    data.get((int)view.getTag()).setUserAnswerIsMetaphor(false);
                    data.get((int)view.getTag()).setUserHasAnswered(((CheckBox) view).isChecked() ? "true" : "");
                    notifyDataSetChanged();
                }
            });

        }
        else {
            holder.cbMetafora.setTag(position);

            holder.cbMetafora.setChecked(data.get(position).isUserAnswerIsMetaphor());

            holder.cbMetafora.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    data.get((int)view.getTag()).setUserAnswerIsMetaphor(((CheckBox) view).isChecked());
                    data.get((int)view.getTag()).setUserHasAnswered(((CheckBox) view).isChecked() ? "true" : "");
                    notifyDataSetChanged();
                }
            });
        }




    }

    public List<Question> getData() {
        return this.data;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvQuestion, tvMetafora, tvKurioleksia;
        CheckBox cbMetafora, cbKurioleksia;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvQuestion = itemView.findViewById(R.id.tv_question);


            if(type == 0) {
                tvMetafora = itemView.findViewById(R.id.tv_metafora);
                tvKurioleksia = itemView.findViewById(R.id.tv_kurioleksia);
            }
            else if(type == 1){
                cbMetafora = itemView.findViewById(R.id.cb_metafora);
                cbKurioleksia = itemView.findViewById(R.id.cb_kurioleksia);
            }
            else {
                cbMetafora = itemView.findViewById(R.id.cb_metafora);
            }
        }
    }
}
