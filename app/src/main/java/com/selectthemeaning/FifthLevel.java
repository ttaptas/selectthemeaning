package com.selectthemeaning;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.selectthemeaning.data.Question;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tasos on 10/03/2018.
 */

public class FifthLevel extends AppCompatActivity {

    private RecyclerView recycler;
    private ThirdLevelAdapter adapter;
    private TextView tvTitle;
    private List<Question> data = new ArrayList<>();
    private Button btnNext;
    private Toolbar toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.third_level);

        toolbar = findViewById(R.id.toolbar);
        tvTitle = findViewById(R.id.tv_title);
        tvTitle.setText("Επιλέγω είτε μεταφορά είτε κυριολεξία");
        recycler = findViewById(R.id.recycler);
        btnNext = findViewById(R.id.btn_next);

        makeData();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeConfirmDialog();
            }
        });

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Πέμπτο επίπεδο");
        }
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    private void getCorrectAnswers() {
        Utils.fifthLevelAnswers = 0;

        for(Question question : data) {
            if(question.isMetaphor() == question.isUserAnswerIsMetaphor() && !question.getUserHasAnswered().equals("")) {
                Utils.fifthLevelAnswers++;
            }
        }

    }

    private void makeConfirmDialog() {
        getCorrectAnswers();
        new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle("Επαλήθευση αποτελεσμάτων")
                .setMessage("Απάντησες σωστά σε " + Utils.fifthLevelAnswers+"/11 ερωτήσεις.\n"+(Utils.fifthLevelAnswers < 6 ? "Θέλεις να προσπαθήσεις ξανά;" : "Θέλεις να προχωρήσεις;") )
                .setPositiveButton("NAI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(Utils.fifthLevelAnswers < 6) {
                        }
                        else{
                            Utils.dataFifthLevel.addAll(data);
                            startActivity(new Intent(FifthLevel.this, SixthLevel.class));
                        }

                    }
                })
                .setNegativeButton("ΟΧΙ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(Utils.fifthLevelAnswers < 6) {
                            Utils.dataFifthLevel.addAll(data);
                            startActivity(new Intent(FifthLevel.this, SixthLevel.class));
                        }
                        else {

                        }
                    }
                })
                .show();
    }


    private void makeData() {
        Utils.fifthLevelAnswers = 0;
        Question question = new Question();
        question.setQuestion("• Δεν παίρνει από λόγια");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Σκοτώνεται στη δουλειά");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Το χρώμα της ζακέτας είναι μωβ");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Έχασε το χρώμα της");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Αγόρασε μια δερμάτινη ζώνη");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Μια πύρινη ζώνη κύκλωσε το σπίτι");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Έφερε στον κόσμο ένα κοριτσάκι");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Έπεσε σε βαθύ ύπνο");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Έπεσε σε βαθιά θάλασσα");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Αυτός σηκώνει πολλά βάρη");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Αυτό σηκώνει πολλή συζήτηση");
        question.setMetaphor(true);

        data.add(question);

        adapter = new ThirdLevelAdapter(data, this, 1);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(mLayoutManager);
        recycler.setAdapter(adapter);

    }




}
