package com.selectthemeaning;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.selectthemeaning.data.Question;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tasos on 10/03/2018.
 */

public class ForthLevel extends AppCompatActivity {

    private RecyclerView recycler;
    private ThirdLevelAdapter adapter;
    private List<Question> data = new ArrayList<>();
    private Button btnNext;
    private Toolbar toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.third_level);

        toolbar = findViewById(R.id.toolbar);
        recycler = findViewById(R.id.recycler);
        btnNext = findViewById(R.id.btn_next);

        makeData();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeConfirmDialog();
            }
        });

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Τέταρτο επίπεδο");
        }
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    private void getCorrectAnswers() {
        Utils.forthLevelAnswers = 0;

        for(Question question : data) {
            if(question.isMetaphor() == question.isUserAnswerIsMetaphor() && !question.getUserHasAnswered().equals("")) {
                Utils.forthLevelAnswers++;
            }
        }

    }

    private void makeConfirmDialog() {
        getCorrectAnswers();
        new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle("Επαλήθευση αποτελεσμάτων")
                .setMessage("Απάντησες σωστά σε " + Utils.forthLevelAnswers+"/16 ερωτήσεις.\n"+(Utils.forthLevelAnswers < 8 ? "Θέλεις να προσπαθήσεις ξανά;" : "Θέλεις να προχωρήσεις;") )
                .setPositiveButton("NAI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(Utils.forthLevelAnswers < 8) {
                        }
                        else{
                            Utils.dataForthLevel.addAll(data);
                            startActivity(new Intent(ForthLevel.this, FifthLevel.class));
                        }

                    }
                })
                .setNegativeButton("ΟΧΙ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(Utils.forthLevelAnswers < 8) {
                            Utils.dataForthLevel.addAll(data);
                            startActivity(new Intent(ForthLevel.this, FifthLevel.class));
                        }
                        else {

                        }
                    }
                })
                .show();
    }


    private void makeData() {
        Utils.forthLevelAnswers = 0;

        Question question = new Question();
        question.setQuestion("• Είναι χρυσό παιδί");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Φορούσε χρυσό ρολόι");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Μου ράγισε η καρδιά με αυτά τα νέα");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Τα χρόνια έσκαψαν το πρόσωπο του παππού");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Τα λόγια του ήταν πικρά");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Το φάρμακο ήταν πικρό");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Η θάλασσα είναι βαθιά");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Το νόημα ήταν βαθύ");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Έχει χαρακτήρα διαμάντι");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Τα μάτια του πετούσαν σπίθες από την οργή");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Η φωτιά πετούσε σπίθες");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Τα λόγια του ήταν αιχμηρά");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Το μαχαίρι είναι αιχμηρό");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Αυτός είναι ξύλο απελέκητο");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Πάτωσα στο διαγώνισμα");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Πάτησα στο έδαφος");
        question.setMetaphor(false);

        data.add(question);

        adapter = new ThirdLevelAdapter(data, this, 0);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(mLayoutManager);
        recycler.setAdapter(adapter);


    }




}
