package com.selectthemeaning;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.selectthemeaning.data.Question;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tasos on 10/03/2018.
 */

public class SixthLevel extends AppCompatActivity {

    private RecyclerView recycler;
    private ThirdLevelAdapter adapter;
    private TextView tvTitle;
    private List<Question> data = new ArrayList<>();
    private Button btnNext;
    private Toolbar toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.third_level);

        toolbar = findViewById(R.id.toolbar);
        tvTitle = findViewById(R.id.tv_title);
        tvTitle.setText("Ποιές από τις παρακάτω φράσεις είναι μεταφορά;");
        recycler = findViewById(R.id.recycler);
        btnNext = findViewById(R.id.btn_next);

        makeData();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeConfirmDialog();
            }
        });

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Έκτο επίπεδο");
        }
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    private void getCorrectAnswers() {
        Utils.sixthLevelAnswers = 0;

        for(Question question : data) {
            if(!question.isMetaphor() || question.isMetaphor() == question.isUserAnswerIsMetaphor()) {
                Utils.sixthLevelAnswers++;
            }
        }

    }

    private void makeConfirmDialog() {
        getCorrectAnswers();
        new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle("Επαλήθευση αποτελεσμάτων")
                .setMessage("Απάντησες σωστά σε " + Utils.sixthLevelAnswers+"/16 ερωτήσεις.\n"+(Utils.sixthLevelAnswers < 8 ? "Θέλεις να προσπαθήσεις ξανά;" : "Θέλεις να προχωρήσεις;") )
                .setPositiveButton("NAI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(Utils.sixthLevelAnswers < 8) {
                        }
                        else{
                            Utils.dataSixthLevel.addAll(data);
                            startActivity(new Intent(SixthLevel.this, FinalLevel.class));
                        }

                    }
                })
                .setNegativeButton("ΟΧΙ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(Utils.sixthLevelAnswers < 8) {
                            Utils.dataSixthLevel.addAll(data);
                            startActivity(new Intent(SixthLevel.this, FinalLevel.class));
                        }
                        else {

                        }
                    }
                })
                .show();
    }


    private void makeData() {
        Utils.sixthLevelAnswers = 0;
        Question question = new Question();
        question.setQuestion("• Καθαρά ρούχα");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Καθαρές κουβέντες");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Κάνω δουλειές");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Κάνω την πάπια");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Μασάω τα λόγια μου");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Μασάω τροφή");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Πέφτω από την καρέκλα");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Πέφτω από τα σύννεφα");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Μαύρη τσάντα");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Μαύρη ζωή");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Έφαγε 2 γκολ");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Έφαγε γεμιστά");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Πικρός καφές");
        question.setMetaphor(false);

        data.add(question);

        question = new Question();
        question.setQuestion("• Πικρά λόγια");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Γλυκιά καρδιά");
        question.setMetaphor(true);

        data.add(question);

        question = new Question();
        question.setQuestion("• Γλυκό μωρό");
        question.setMetaphor(false);

        data.add(question);

        adapter = new ThirdLevelAdapter(data, this, 2);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(mLayoutManager);
        recycler.setAdapter(adapter);

    }




}