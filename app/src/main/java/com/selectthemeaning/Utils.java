package com.selectthemeaning;

import com.selectthemeaning.data.MetaphorQuestion;
import com.selectthemeaning.data.Question;
import com.selectthemeaning.data.QuestionWithAnswers;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tasos on 08/03/2018.
 */

public class Utils {

    public static int firstLevelAnswers;
    public static int secondLevelAnswers;
    public static int thirdLevelAnswers;
    public static int forthLevelAnswers;
    public static int fifthLevelAnswers;
    public static int sixthLevelAnswers;
    public static int finalLevelAnswers;

    public static List<MetaphorQuestion> dataFirstLevel = new ArrayList<>();
    public static List<QuestionWithAnswers> dataSecondLevel = new ArrayList<>();
    public static List<Question> dataThirdLevel = new ArrayList<>();
    public static List<Question> dataForthLevel = new ArrayList<>();
    public static List<Question> dataFifthLevel = new ArrayList<>();
    public static List<Question> dataSixthLevel = new ArrayList<>();
    public static List<QuestionWithAnswers> dataFinalLevel = new ArrayList<>();
}
