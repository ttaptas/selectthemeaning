package com.selectthemeaning;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.selectthemeaning.data.MetaphorQuestion;
import com.selectthemeaning.data.QuestionWithAnswers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Tasos on 08/03/2018.
 */

public class SecondLevel extends AppCompatActivity {

    private Button btnNext;
    private TextView tvFirst, tvSecond, tvThird, tvForth, tvFifth;
    private TextView tvFirstAnswer, tvSecondAnswer, tvThirdAnswer, tvForthAnswer, tvFifthAnswer;

    List<QuestionWithAnswers> data = new ArrayList<>();
    List<String> possibleAnswers = new ArrayList<>();

    private int preSelectedAnswer;
    private CharSequence[] answers;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_level);

        makeData();

        toolbar = findViewById(R.id.toolbar);
        btnNext = findViewById(R.id.btn_next);
        tvFirst = findViewById(R.id.tv_first);
        tvFirstAnswer = findViewById(R.id.tv_first_answer);
        tvFirstAnswer.setTag(tvFirst.getText().toString());
        tvFirstAnswer.setOnClickListener(answerListener);

        tvSecond = findViewById(R.id.tv_second);
        tvSecondAnswer = findViewById(R.id.tv_second_answer);
        tvSecondAnswer.setTag(tvSecond.getText().toString());
        tvSecondAnswer.setOnClickListener(answerListener);

        tvThird = findViewById(R.id.tv_third);
        tvThirdAnswer = findViewById(R.id.tv_third_answer);
        tvThirdAnswer.setTag(tvThird.getText().toString());
        tvThirdAnswer.setOnClickListener(answerListener);

        tvForth = findViewById(R.id.tv_forth);
        tvForthAnswer = findViewById(R.id.tv_forth_answer);
        tvForthAnswer.setTag(tvForth.getText().toString());
        tvForthAnswer.setOnClickListener(answerListener);

        tvFifth = findViewById(R.id.tv_fifth);
        tvFifthAnswer = findViewById(R.id.tv_fifth_answer);
        tvFifthAnswer.setTag(tvFifth.getText().toString());
        tvFifthAnswer.setOnClickListener(answerListener);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeConfirmDialog();
            }
        });

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Δεύτερο επίπεδο");
        }
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    private View.OnClickListener answerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            makeAnswerDialog((TextView) v, ((String) v.getTag()));
        }
    };

    private void getCorrectAnswers() {
        Utils.secondLevelAnswers = 0;
        for(QuestionWithAnswers questionObj : data) {
            if(questionObj.getUserAnswer().equals(questionObj.getCorrectAnswer())) {
                Utils.secondLevelAnswers++;
            }
        }

    }

    private void makeConfirmDialog() {
        getCorrectAnswers();
        new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle("Επαλήθευση αποτελεσμάτων")
                .setMessage("Απάντησες σωστά σε " + Utils.secondLevelAnswers+"/5 ερωτήσεις.\n"+(Utils.secondLevelAnswers < 3 ? "Θέλεις να προσπαθήσεις ξανά;" : "Θέλεις να προχωρήσεις;") )
                .setPositiveButton("NAI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(Utils.secondLevelAnswers < 3) {
                        }
                        else{
                            Utils.dataSecondLevel.addAll(data);
                            Log.e("Επόμενο", "βήμα");
                            startActivity(new Intent(SecondLevel.this, ThirdLevel.class));
                        }

                    }
                })
                .setNegativeButton("ΟΧΙ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(Utils.secondLevelAnswers < 3) {
                            Utils.dataSecondLevel.addAll(data);
                            Log.e("Επόμενο", "βήμα");
                            startActivity(new Intent(SecondLevel.this, ThirdLevel.class));
                        }
                        else {

                        }
                    }
                })
                .show();
    }

    private void setPossibleAnswers(TextView view) {
        possibleAnswers.clear();
        for(QuestionWithAnswers questionWithAnswers : data) {
            if(questionWithAnswers.getQuestion().equals(view.getTag())) {
                possibleAnswers.addAll(questionWithAnswers.getAnswers());
            }
        }

        Collections.shuffle(possibleAnswers);
        findPreselectedAnswer(view.getText().toString());
        answers = possibleAnswers.toArray(new CharSequence[possibleAnswers.size()]);
    }

    private void findPreselectedAnswer(String input) {
        for (int i = 0; i < possibleAnswers.size(); i++) {
            if (possibleAnswers.get(i).equals(input)) {
                preSelectedAnswer = i;
                return;
            }
        }

        preSelectedAnswer = -1;
    }

    private void makeAnswerDialog(final TextView view, final String question) {
        setPossibleAnswers(view);

        new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle(question.replace("• ", ""))
                .setSingleChoiceItems(answers, preSelectedAnswer, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        preSelectedAnswer = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        view.setText(answers[preSelectedAnswer].toString());
                        validateData(question, answers[preSelectedAnswer].toString());
                    }
                })
                .show();
    }

    private void validateData(String question, String answer) {
        for(QuestionWithAnswers questionObj : data) {
            if(questionObj.getQuestion().equals(question)) {
                questionObj.setUserAnswer(answer);
                break;
            }
        }
    }


    private void makeData() {
        Utils.secondLevelAnswers = 0;

        QuestionWithAnswers questionWithAnswers = new QuestionWithAnswers();
        List<String> answers = new ArrayList<>();
        questionWithAnswers.setQuestion("• Ψήνεται στον πυρετό.");
        answers.add("Έχει πολύ πυρετό");
        answers.add("Όταν έχει πυρετό του ψήνουν φαγητό");
        questionWithAnswers.setAnswers(answers);
        questionWithAnswers.setCorrectAnswer("Έχει πολύ πυρετό");

        data.add(questionWithAnswers);

        questionWithAnswers = new QuestionWithAnswers();
        answers = new ArrayList<>();
        questionWithAnswers.setQuestion("• Έχει τις μαύρες του.");
        answers.add("Έχει ντυθεί στα μαύρα");
        answers.add("Είναι στεναχωρημένος");
        questionWithAnswers.setAnswers(answers);
        questionWithAnswers.setCorrectAnswer("Έχει ντυθεί στα μαύρα");

        data.add(questionWithAnswers);

        questionWithAnswers = new QuestionWithAnswers();
        answers = new ArrayList<>();
        questionWithAnswers.setQuestion("• Του έταξε τον ουρανό με τ' άστρα.");
        answers.add("Του έταξε πολλά");
        answers.add("Του έταξε μια αφίσα με τον ουρανό και τ' άστρα");
        questionWithAnswers.setAnswers(answers);
        questionWithAnswers.setCorrectAnswer("Του έταξε πολλά");

        data.add(questionWithAnswers);

        questionWithAnswers = new QuestionWithAnswers();
        answers = new ArrayList<>();
        questionWithAnswers.setQuestion("• Δε χαρίζει κάστανα.");
        answers.add("Δεν κάνει εύκολα χάρες");
        answers.add("Δεν χάρισε κάστανα να φάνε");
        questionWithAnswers.setAnswers(answers);
        questionWithAnswers.setCorrectAnswer("Δεν κάνει εύκολα χάρες");

        data.add(questionWithAnswers);

        questionWithAnswers = new QuestionWithAnswers();
        answers = new ArrayList<>();
        questionWithAnswers.setQuestion("• Δεν έβγαζα τα γράμματά σου.");
        answers.add("Δεν έβγαζα τα γράμματά σου από το γραμματοκιβώτιο");
        answers.add("Δεν μπόρεσα να διαβάσω τα γράμματά σου");
        questionWithAnswers.setAnswers(answers);
        questionWithAnswers.setCorrectAnswer("Δεν μπόρεσα να διαβάσω τα γράμματά σου");

        data.add(questionWithAnswers);

    }
}
