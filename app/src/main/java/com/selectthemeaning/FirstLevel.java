package com.selectthemeaning;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.selectthemeaning.data.MetaphorQuestion;
import com.selectthemeaning.data.Question;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Tasos on 04/03/2018.
 */

public class FirstLevel extends AppCompatActivity {

    private Button btnNext;
    private TextView tvFirst, tvSecond, tvThird, tvForth, tvFifth, tvSixth, tvSeventh, tvEighth;
    private TextView tvFirstAnswer, tvSecondAnswer, tvThirdAnswer, tvForthAnswer, tvFifthAnswer,
            tvSixthAnswer, tvSeventhAnswer, tvEighthAnswer;

    List<MetaphorQuestion> data = new ArrayList<>();
    List<String> possibleAnswers = new ArrayList<>();
    private int preSelectedAnswer;
    private CharSequence[] answers;
    private Toolbar toolbar;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_level);

        makeData();

        toolbar = findViewById(R.id.toolbar);
        btnNext = findViewById(R.id.btn_next);
        tvFirst = findViewById(R.id.tv_first);
        tvFirstAnswer = findViewById(R.id.tv_first_answer);
        tvFirstAnswer.setTag(tvFirst.getText().toString());
        tvFirstAnswer.setOnClickListener(answerListener);

        tvSecond = findViewById(R.id.tv_second);
        tvSecondAnswer = findViewById(R.id.tv_second_answer);
        tvSecondAnswer.setTag(tvSecond.getText().toString());
        tvSecondAnswer.setOnClickListener(answerListener);

        tvThird = findViewById(R.id.tv_third);
        tvThirdAnswer = findViewById(R.id.tv_third_answer);
        tvThirdAnswer.setTag(tvThird.getText().toString());
        tvThirdAnswer.setOnClickListener(answerListener);

        tvForth = findViewById(R.id.tv_forth);
        tvForthAnswer = findViewById(R.id.tv_forth_answer);
        tvForthAnswer.setTag(tvForth.getText().toString());
        tvForthAnswer.setOnClickListener(answerListener);

        tvFifth = findViewById(R.id.tv_fifth);
        tvFifthAnswer = findViewById(R.id.tv_fifth_answer);
        tvFifthAnswer.setTag(tvFifth.getText().toString());
        tvFifthAnswer.setOnClickListener(answerListener);

        tvSixth = findViewById(R.id.tv_sixth);
        tvSixthAnswer = findViewById(R.id.tv_sixth_answer);
        tvSixthAnswer.setTag(tvSixth.getText().toString());
        tvSixthAnswer.setOnClickListener(answerListener);

        tvSeventh = findViewById(R.id.tv_seventh);
        tvSeventhAnswer = findViewById(R.id.tv_seventh_answer);
        tvSeventhAnswer.setTag(tvSeventh.getText().toString());
        tvSeventhAnswer.setOnClickListener(answerListener);

        tvEighth = findViewById(R.id.tv_eighth);
        tvEighthAnswer = findViewById(R.id.tv_eighth_answer);
        tvEighthAnswer.setTag(tvEighth.getText().toString());
        tvEighthAnswer.setOnClickListener(answerListener);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeConfirmDialog();
            }
        });

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Πρώτο επίπεδο");
        }
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }


    private View.OnClickListener answerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            makeAnswerDialog((TextView) v, ((String) v.getTag()));
        }
    };

    private void setPossibleAnswers(TextView view) {
        possibleAnswers.clear();
        for (MetaphorQuestion question : data) {
            possibleAnswers.add(question.getCorrectMetaphor());
        }

        Collections.shuffle(possibleAnswers);
        findPreselectedAnswer(view.getText().toString());
        answers = possibleAnswers.toArray(new CharSequence[possibleAnswers.size()]);

    }

    private void findPreselectedAnswer(String input) {
        for (int i = 0; i < possibleAnswers.size(); i++) {
            if (possibleAnswers.get(i).equals(input)) {
                preSelectedAnswer = i;
                return;
            }
        }

        preSelectedAnswer = -1;
    }

    private void getCorrectAnswers() {
        Utils.firstLevelAnswers = 0;
        for(MetaphorQuestion questionObj : data) {
            if(questionObj.getUserAnswer().equals(questionObj.getCorrectMetaphor())) {
                Utils.firstLevelAnswers++;
            }
        }

    }

    private void makeConfirmDialog() {
        getCorrectAnswers();
        new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle("Επαλήθευση αποτελεσμάτων")
                .setMessage("Απάντησες σωστά σε " + Utils.firstLevelAnswers+"/8 ερωτήσεις.\n"+(Utils.firstLevelAnswers < 5 ? "Θέλεις να προσπαθήσεις ξανά;" : "Θέλεις να προχωρήσεις;") )
                .setPositiveButton("NAI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(Utils.firstLevelAnswers < 5) {
                        }
                        else{
                            Utils.dataFirstLevel.addAll(data);
                            startActivity(new Intent(FirstLevel.this, SecondLevel.class));

                        }

                    }
                })
                .setNegativeButton("ΟΧΙ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(Utils.firstLevelAnswers < 5) {
                            Utils.dataFirstLevel.addAll(data);
                            startActivity(new Intent(FirstLevel.this, SecondLevel.class));
                        }
                        else {

                        }
                    }
                })
                .show();
    }

    private void makeAnswerDialog(final TextView view, final String question) {
        setPossibleAnswers(view);

        new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle(question.replace("• ", ""))
                .setSingleChoiceItems(answers, preSelectedAnswer, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        preSelectedAnswer = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        view.setText(answers[preSelectedAnswer].toString());
                        validateData(question, answers[preSelectedAnswer].toString());
                    }
                })
                .show();
    }


//    private void resetData() {
//        tvFirstAnswer.setText("");
//        tvSecondAnswer.setText("");
//        tvThirdAnswer.setText("");
//        tvForthAnswer.setText("");
//        tvFifthAnswer.setText("");
//        tvSixthAnswer.setText("");
//        tvSeventhAnswer.setText("");
//        tvEighthAnswer.setText("");
//
//        Utils.firstLevelAnswers = 0;
//
//        for(MetaphorQuestion questionObj : data) {
//                questionObj.setUserAnswer("");
//        }
//
//    }

    private void validateData(String question, String answer) {
        for(MetaphorQuestion questionObj : data) {
            if(questionObj.getQuestion().equals(question)) {
                questionObj.setUserAnswer(answer);
                break;
            }
        }
    }

    private void makeData() {
        Utils.firstLevelAnswers = 0;

        MetaphorQuestion question = new MetaphorQuestion();
        question.setQuestion("• Τα φορτώνω στον κόκορα.");
        question.setCorrectMetaphor("Τεμπελιάζω");
        data.add(question);

        question = new MetaphorQuestion();
        question.setQuestion("• Το παίζω στα δάχτυλα.");
        question.setCorrectMetaphor("Ξέρω κάτι καλά");
        data.add(question);

        question = new MetaphorQuestion();
        question.setQuestion("• Ψάχνω ψύλλους στ' άχυρα.");
        question.setCorrectMetaphor("Ψάχνω κάτι που είναι δύσκολο να βρω");
        data.add(question);

        question = new MetaphorQuestion();
        question.setQuestion("• Δε χαλάω τη ζαχαρένια μου.");
        question.setCorrectMetaphor("Δε στεναχωριέμαι πολύ");
        data.add(question);

        question = new MetaphorQuestion();
        question.setQuestion("• Ανακατεύομαι στα πόδια του.");
        question.setCorrectMetaphor("Μπερδεύομαι στις δουλειές του");
        data.add(question);

        question = new MetaphorQuestion();
        question.setQuestion("• Μην παίζεις μαζί μου.");
        question.setCorrectMetaphor("Μη με κοροϊδεύεις");
        data.add(question);

        question = new MetaphorQuestion();
        question.setQuestion("• Τα νιάτα φεύγουν νεράκι.");
        question.setCorrectMetaphor("Περνούν γρήγορα τα χρόνια");
        data.add(question);

        question = new MetaphorQuestion();
        question.setQuestion("• Αυτό το νέο δεν μπορώ να το χωνέψω.");
        question.setCorrectMetaphor("Μου είναι δύσκολο να το δεχτώ");
        data.add(question);
    }
}
